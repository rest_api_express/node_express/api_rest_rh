const express = require('express')
const { connectTodDB, getDB } = require('./db')

const app = express()

const port = process.env.PORT || 3000

// Middlewares

// DB connection
let db
connectTodDB((err) => {
    if (!err) {
        app.listen(port, () => {
            console.log('run server at 3000')
        })
        db = getDB()
    } else {
        console.log('error :', err)
    }
})

// Routes

/** */

app.get('/users', async (req, res) => {
    const data = await db.collection('user').find({}).toArray()
    try {
        res.status(200).json(data)
    } catch (error) {
        res.status(500).json({ error: 'Could not fetch users' })
    }
})
