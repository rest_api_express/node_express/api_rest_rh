const { MongoClient } = require('mongodb')
require('dotenv').config()

let dbConnection

module.exports = {
    connectTodDB: (callback) => {
        MongoClient.connect(process.env.DB_CONNECTION)
            .then((client) => {
                dbConnection = client.db()
                return callback()
            })
            .catch((err) => {
                console.log(err)
                return callback(err)
            })
    },
    getDB: () => dbConnection,
}
